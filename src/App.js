import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Row,Col,Grid} from 'react-bootstrap';
import MarketDominationChart from './MarketDominationChart'

function App() {
  return (
    <div className="App">
      <Grid fluid>
        <Row>
          <Col xs={6} xsOffset={3}>
            <MarketDominationChart/>
          </Col>
        </Row>
      </Grid>
    </div>
  );
}

export default App;
