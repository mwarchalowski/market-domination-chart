import React from 'react';
import * as d3 from 'd3'
import {areaLabel} from 'd3-area-label'

class MarketDominationArea extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      labelTransform: 'translate(0px, 0px)'
    }
  }

  componentDidUpdate(prev) {
    const {areaGen, dataPoints} = this.props
    if(prev.dataPoints === dataPoints) {
      return
    }
    const areaLabelGenerator = areaLabel(areaGen)
    const res = areaLabelGenerator.call(this.label, dataPoints)
    // this.setState({labelTransform: `translate(${res.x}px, ${res.y}px) scale(${res.scale})`})
    this.setState({labelTransform: res.toString()})
  }

  getLabelStyle = () => {
    return {
      fill: 'white'
    }
  }

  render() {
    const {id, path, fill, textPath} = this.props
    return (
      <g>
        <path d={path} stroke={fill} fill={fill}></path>
        <text
          ref={ref => this.label = ref}
          style={this.getLabelStyle()}
          transform={this.state.labelTransform}
          >
            {id}
          </text>
        {/* <text ref={ref => this.label = ref} >{id}</text> */}
        {/* <path id={id} d={textPath} stroke={'none'} fill={'none'} />
        <text className="area-label">
          <textPath
            href={`#${id}`}
            startOffset="15px"
            >
            {id}
          </textPath>
        </text> */}
      </g>
    )
  }
}

export default MarketDominationArea