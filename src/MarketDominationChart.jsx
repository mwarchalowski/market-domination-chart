import React from 'react';
import { ResponsiveContainer } from 'recharts';
import {Grid, Row, Col} from 'react-bootstrap';
import * as d3 from 'd3';
import {areaLabel} from 'd3-area-label'
import _ from 'underscore';
import allData from './data';
import MarketDominationArea from './MarketDominationArea'
import { isCallChain } from 'typescript';

class MarketDominationChart extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      orgsCount: 25,
      gridWidth: 0,
      gridHeight: 0
    }
  }

  // transpose = ar => {
  //   let transposed = []
  //   arr.forEach((row, i) => {
  //     row.forEach((value, j) => {
  //       transposed[j] = transposed[j] || []
  //       transposed[j][i] = value;
  //     })
  //   })
  //   return transposed;
  // }

  gridSizeChanged = ({width, height}) => {
    const {gridWidth, gridHeight}  = this.state
    return width !== gridWidth || height !== gridHeight
  }

  componentWillUnmount() {
    this.gridObserver.disconnect()
  }

  componentDidMount() {
    this.gridObserver = new ResizeObserver(([svgObservable]) => {
      const {width, height} = this.gridArea.getBoundingClientRect()
      if(this.gridSizeChanged({width, height})) {
        this.setState({gridWidth: width, gridHeight: height})
      }
    })
    this.gridObserver.observe(this.gridArea);
  }

  // chartPaths = ({rows, columns}) => {
  //   const {gridWidth, gridHeight} = this.state
  //   const yScale = d3.scaleLinear().domain([0, columns.length]).range([0, gridWidth])
  //   const xScale = d3.scaleLinear().domain([0, 100]).range([0, gridHeight])

  //   const bottomRow : DataRow = columns.map(col => 0);
  //   // const initialCollector : [DataRow, DataRow[]] = [bottomRow, []]
  // }

  updateOrgsCount = (e) => {
    this.setState({orgsCount: parseInt(e.target.value)})
  }

  render() {
    const _d3 = d3
    const areaL = areaLabel
    debugger;
    const insightData = allData;
    const filteredData = insightData.slice(0, this.state.orgsCount)

    const orgs = filteredData.map(({profile}) => { return profile.label});

    const dataByYear = filteredData.reduce((acc, {profile, data}) => {
      data.forEach(({year, value}) => {
        acc[year] = acc[year] || {}
        acc[year][profile.label] = value;
      });
      return acc;
    }, {});

    const years = Object.keys(dataByYear).map(m => parseInt(m))
    const stackData = years.map((year) => {
      return {year: year, ...dataByYear[year]}
    })

    const color = d3.scaleOrdinal(d3.schemeCategory10);
    const colorMap = orgs.reduce((acc, org, i) => {
      return {[org]: color(org), ...acc}
    },{})

    // let rows = insightData.map((companyData) : DataRow => companyData.data.map(dataPoint => dataPoint.value));
    // let columns = this.transpose(rows);

    // const totals = columns.map((column) => column.reduce((acc, value) => acc + value, 0));
    // rows = rows.map((row) => row.map((value, index) => value / totals[index]))

    const xScale = d3.scaleLinear().domain(d3.extent(years)).range([0, this.state.gridWidth])
    const yScale = d3.scaleLinear().domain([0, 1]).range([this.state.gridHeight, 0])
    const stack = d3.stack().keys(orgs).offset(d3.stackOffsetExpand)
    const series = stack(stackData)
    let areaGen = d3.area()
      .x((d) => {
        let x = xScale(d.data.year)
        return x
      })
      .y0((d) => {
        let y = yScale(d[0])
        return y
      })
      .y1((d) => {
        let y = yScale(d[1])
        return y
      });
    const areas = series.map(s => {
      const path = areaGen(s)
      // const text = d3.create("svg").append("text").text("DUPA").node()

      const identifier = s.key
      const percentByYear = s.reduce((acc, yearData) => {
        const [low, high] = yearData;
        return {...acc, [yearData.data.year]: [high - low] }
      }, {})
      const [firstPoint, ...remainingPoints] = s.map(yearData => {
        const [low, high] = yearData;
        return [xScale(yearData.data.year), yScale(low)]
      })
      const textPath = d3.path()
      textPath.moveTo(firstPoint[0], firstPoint[1])
      remainingPoints.forEach(([x, y]) => {
        textPath.lineTo(x,y)
      })
      return (
        <MarketDominationArea
          path={path}
          textPath={textPath}
          id={identifier}
          data={percentByYear}
          fill={colorMap[identifier]}
          areaGen={areaGen}
          dataPoints={s}
          />
      )
    })

    const yAxis = d3.axisLeft(yScale).ticks(10, "%")
    yAxis(d3.select(this.yAxisLayer))

    const xAxis = d3.axisBottom(xScale).ticks(10, ".0f")
    xAxis(d3.select(this.xAxisLayer))

    const polyScale = d3.scaleLinear().domain([0, series.length]).range([this.state.gridHeight, 0])
    const polyLines = series.map((s,index) => {
      const lastPoint = yScale(s[s.length - 1][0])

      const pathBreak = 30
      const pathEnd = 100
      const pathProps = {fill: 'none', strokeWidth: '0.25px'}

      const linkPathBuilder = d3.path()
      linkPathBuilder.moveTo(0, lastPoint)
      linkPathBuilder.lineTo(pathBreak, polyScale(index))
      const linkPath = <path d={linkPathBuilder.toString()} {...pathProps}/>

      const pathBuilder = d3.path()
      pathBuilder.moveTo(pathBreak, polyScale(index))
      pathBuilder.lineTo(pathEnd, polyScale(index))
      const tPath = <path id={`side-path-${s.key}`} d={pathBuilder.toString()} {...pathProps}/>
      return (
        <g>
          <circle cx={0} cy={lastPoint} r="2px" fill="white" strokeWidth="0.25px"></circle>
          {linkPath}
          {tPath}
          <text className="side-label">
            <textPath href={`#side-path-${s.key}`}>
              {s.key}
            </textPath>
          </text>
        </g>
        // <polyline
        //   strokeWidth="0.25px"
        //   fill="none"
        //   points={`0,${lastPoint} 30,${polyScale(index)} 100,${polyScale(index)}`}
        //   />
      )
    })

    return (
      <Grid fluid>
        <Row>
          <Col xs={12} style={{height: '80vh'}}>
            <div className="sub-section-insights" >
              <div className="sub-section-heading">
                Market Domination Chart <i className="fa fa-info-circle" data-toggle="tooltip"/>
                <input type="range" min="2" max={25} onChange={this.updateOrgsCount}/>
              </div>
            </div>

            <div
              className="market-domination-chart-container"
              style={{width: '100%', height:'100%'}}
              ref={ref => this.container = ref}
              >
              <svg width="100%" height="100%">
                <svg width={'10%'} y="5%" height={'90%'} style={{overflow: 'visible'}}>
                  <g ref={ref => {this.yAxisLayer = ref}} style={{transform: 'translate(95%, 0)'}}></g>
                </svg>
                <svg width="85%" height="5%" y="95%" x="10%" style={{overflow: 'visible'}}>
                  <g ref={ref => {this.xAxisLayer = ref}} style={{transform: 'translate(0, 10%)'}}></g>
                </svg>
                <svg width={'70%'} x={'10%'} y="5%" height={'90%'} >
                  <rect width="100%" height="100%" fill="white" ref={ref => {this.gridArea = ref}}></rect>
                  {areas.map((area) => area)}
                </svg>
                <svg width={'15%'} y={'5%'} height={'90%'} x={'80%'} style={{overflow: 'visible'}}>
                  <g stroke="black">
                    {polyLines.map(p => p)}
                  </g>
                </svg>
              </svg>
              <div style={{width: "80px"}}>Legend</div>
            </div>
          </Col>
        </Row>
      </Grid>
    )
  }
}

export default MarketDominationChart