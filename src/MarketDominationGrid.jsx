import React from 'react'

class MarketDominationGrid extends React.Component {

  gridElement = null;

  componentDidMount = () => {
    debugger;
  }

  componentDidUpdate = () => {
    debugger;
  }

  render() {
    const {width, height, x, onResize} = this.props
    return (
      <svg {...width, height, x} ref={ref => this.gridElement = ref}>
        <rect width="100%" height="100%" fill="white"></rect>
        {children}
      </svg>
    )
  }
}

export default MarketDominationGrid